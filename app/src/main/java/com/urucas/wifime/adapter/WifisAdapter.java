package com.urucas.wifime.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.urucas.wifime.R;
import com.urucas.wifime.activities.MainActivity;
import com.urucas.wifime.model.Wifi;

import java.util.ArrayList;

/**
 * Created by Urucas on 8/23/14.
 */
public class WifisAdapter extends ArrayAdapter<Wifi> {

    private ArrayList<Wifi> _wifis;
    private MainActivity _activity;

    public WifisAdapter(Context context, int resource, ArrayList<Wifi> wifis) {
        super(context, resource, wifis);
        _wifis = wifis;
        _activity = (MainActivity) context;
    }

    public void addAll(ArrayList<Wifi> wifis) {
        _wifis = wifis;
        for(int i=0; i<wifis.size(); i++) {
            insert(wifis.get(i), getCount());
        }
    }

    private static class ViewHolder{

        public TextView wifiName;
        public TextView wifiPrettyName;
        public TextView wifiMatched;
        public ImageButton wifiConnectBtt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) this.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = convertView;
        if(rowView == null) {

            rowView = inflater.inflate(R.layout.adapter_wifi_item, parent, false);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.wifiName = (TextView) rowView.findViewById(R.id.wifiName);
            viewHolder.wifiPrettyName = (TextView) rowView.findViewById(R.id.wifiPrettyName);
            viewHolder.wifiMatched = (TextView) rowView.findViewById(R.id.wifiMtach);
            viewHolder.wifiConnectBtt = (ImageButton) rowView.findViewById(R.id.connectBtt);

            rowView.setTag(viewHolder);
        }

        final Wifi w = getItem(position);

        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.wifiName.setText(w.getSsid());

        int matched = w.isMatched() ? R.string.wifimatched : R.string.wifinotavailable;
        String color = w.isMatched() ? "#4B77FF" : "#FF4D4D";
        holder.wifiMatched.setText(getContext().getResources().getString(matched));
        holder.wifiMatched.setTextColor(Color.parseColor(color));

        if(w.getName() != null) {
            holder.wifiPrettyName.setText(w.getName());
            holder.wifiPrettyName.setVisibility(View.VISIBLE);
        }

        if(w.isMatched()) {
            holder.wifiConnectBtt.setImageResource(R.drawable.circle_go);
            holder.wifiConnectBtt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _activity.connect(w.getSsid());
                }
            });
        }else {
            holder.wifiConnectBtt.setImageResource(R.drawable.circle_lock);
            holder.wifiConnectBtt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _activity.addWifiConfig(w);
                }
            });
        }

        return rowView;
    }
}
