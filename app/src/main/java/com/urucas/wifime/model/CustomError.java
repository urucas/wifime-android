package com.urucas.wifime.model;

import android.content.Context;

import com.urucas.wifime.R;
import com.urucas.wifime.WifiApplication;

import java.util.HashMap;

/**
 * @copyright Urucas
 * @license   Copyright (C) 2013. All rights reserved
 * @link       http://urucas.com
 * @developers Bruno Alassia, Pamela Prosperi
 * @date {5/28/14}
**/
public class CustomError {

    private String code;

    private static HashMap<String, Integer> localizedErrors = new HashMap<String, Integer>();

    public CustomError(String code){
        localizedErrors.put("INVALID_JSON_RESPONSE", R.string.invalid_json_response);
        localizedErrors.put("NO_CONNECTION", R.string.no_connection);
        this.setCode(code);
    }

    public String getCode() {
        return code;
    }

    public String getLocalizedMessage(){
        Context context = WifiApplication.getInstance().getApplicationContext();
        try {
            int sid = localizedErrors.get(this.code);
            String msg = context.getResources().getString(sid);
            if(msg != null) {
                return msg;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
