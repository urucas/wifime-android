package com.urucas.wifime.model;

/**
 * Created by Urucas on 8/23/14.
 */
public class Wifi {

    private String name, ssid, pass, type, uuid, addr, bssid;
    private Double lat, lng;
    private boolean matched = false;

    public Wifi(String ssid) {
        this.setSsid(ssid);
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getBssid() {
        return this.bssid;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public boolean isMatched() {
        return matched;
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type.equals("WEP") ? "WEP" : "WPA";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getAddr() {
        return addr;
    }

}
