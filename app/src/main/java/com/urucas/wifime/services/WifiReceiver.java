package com.urucas.wifime.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.urucas.wifime.WifiApplication;

/**
 * Created by Urucas on 8/23/14.
 */
public class WifiReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);

        if (info != null) {
            if (info.isConnected()) {
                WifiApplication.getWifiConnectionCallback().onSuccess();
            }else if(info.isFailover()) {
                WifiApplication.getWifiConnectionCallback().onError();
            }
        }
    }
}
