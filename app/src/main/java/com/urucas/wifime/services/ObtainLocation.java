package com.urucas.wifime.services;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.urucas.wifime.interfaces.AddressCallback;

import java.io.IOException;
import java.util.List;

/**
 * Created by Urucas on 8/25/14.
 */
public class ObtainLocation extends AsyncTask<Void, Void, String> {

    private final AddressCallback _callback;
    private LatLng _latLng;
    private Activity _activity;

    public ObtainLocation(Activity activity, LatLng latLng, AddressCallback callback) {
        _latLng = latLng;
        _activity = activity;
        _callback = callback;
    }

    @Override
    protected String doInBackground(Void... params) {
        Geocoder geoCoder = new Geocoder(_activity);
        List<Address> matches;
        try {
            matches = geoCoder.getFromLocation(_latLng.latitude, _latLng.longitude, 1);
            Address bestMatch = (matches.isEmpty() ? null : matches.get(0));
            if(bestMatch != null) {
                return bestMatch.getAddressLine(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(final String result) {
        super.onPostExecute(result);
        if (result == null) {
            _activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    _callback.onSuccess(result);
                }
            });
            return;
        }
        _activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _callback.onSuccess(result);
            }
        });
    }
}
