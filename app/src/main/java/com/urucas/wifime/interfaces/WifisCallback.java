package com.urucas.wifime.interfaces;

import com.urucas.wifime.model.CustomError;
import com.urucas.wifime.model.Wifi;

import java.util.ArrayList;

/**
 * Created by Urucas on 8/23/14.
 */
public interface WifisCallback {

    public void onSucccess(ArrayList<Wifi> wifis);
    public void onError(CustomError error);

}
