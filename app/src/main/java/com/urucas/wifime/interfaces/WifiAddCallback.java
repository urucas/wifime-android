package com.urucas.wifime.interfaces;

import com.urucas.wifime.model.CustomError;
import com.urucas.wifime.model.Wifi;

/**
 * Created by Urucas on 8/25/14.
 */
public interface WifiAddCallback {

    public void onSuccess(Wifi w);
    public void onError(CustomError error);
}
