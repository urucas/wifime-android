package com.urucas.wifime.interfaces;

import android.location.Address;

/**
 * Created by Urucas on 8/25/14.
 */
public interface AddressCallback {

    public void onSuccess(Address addr);
    public void onSuccess(String addr);
    public void onError();
}
