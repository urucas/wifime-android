package com.urucas.wifime.interfaces;

/**
 * Created by Urucas on 8/23/14.
 */
public interface WifiConnectionCallback {

    public void onSuccess();
    public void onError();
}
