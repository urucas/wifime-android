package com.urucas.wifime;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;


import android.provider.Settings;
import android.provider.Settings.Secure;
import android.util.Log;



import com.urucas.wifime.controller.ApiController;
import com.urucas.wifime.interfaces.WifiConnectionCallback;
import com.urucas.wifime.model.Wifi;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Urucas on 8/18/14.
 */

public class WifiApplication extends Application {

    private static WifiApplication _instance;
    private static ApiController _api;

    private static HashMap<String, Wifi> _wifis;
    private static WifiManager _wifiManager;
    private static WifiConnectionCallback _connectionCallback;
    private static SharedPreferences _prefs;

    public WifiApplication() {
        super();
        _instance = this;
    }

    public static SharedPreferences getSharedPreferences() {
        if(_prefs == null) {
            _prefs = WifiApplication.getInstance().getApplicationContext().getSharedPreferences(
                    "com.urucas.wifime", Context.MODE_PRIVATE);
        }
        return _prefs;
    }

    public static void savePreference(String pref, String value) {
        SharedPreferences prefs = WifiApplication.getSharedPreferences();
        prefs.edit().putString(pref, value).commit();
    }

    public static String getStringPreference(String pref) {
        SharedPreferences prefs = WifiApplication.getSharedPreferences();
        return prefs.getString(pref, "");
    }

    public static void savePreference(String pref, boolean value) {
        SharedPreferences prefs = WifiApplication.getSharedPreferences();
        prefs.edit().putBoolean(pref, value).commit();
    }

    public static boolean getBooleanPreference(String pref) {
        SharedPreferences prefs = WifiApplication.getSharedPreferences();
        return prefs.getBoolean(pref, false);
    }

    public static WifiManager getWifiManager() {
        if(_wifiManager == null) {
            _wifiManager = (WifiManager) _instance.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (_wifiManager.isWifiEnabled() == false) {
                _wifiManager.setWifiEnabled(true);

            }
        }
        return _wifiManager;
    }

    public static ArrayList<Wifi> getWifisList() {
        ArrayList<Wifi> ws = new ArrayList<Wifi>();
        HashMap<String, Wifi> wifis = WifiApplication.getWifis();
        for(String key: wifis.keySet()) {
            ws.add(WifiApplication.getWifis().get(key));
        }
        return ws;
    }

    public static HashMap<String, Wifi> getWifis() {
        if(_wifis == null) {
            _wifis = new HashMap<String, Wifi>();
        }
        return _wifis;
    }

    public static Wifi getWifiConfiguration(String ssid) {
        return WifiApplication.getWifis().get(ssid);
    }

    public static void setWifiConfiguration(String ssid, Wifi wifi) {
        WifiApplication.getWifis().put(ssid, wifi);
    }

    public static boolean connect(String ssid) {
        Wifi wifi = WifiApplication.getWifis().get(ssid);
        if(wifi == null) {
            return false;
        }
        if(wifi.getType().equals("WPA")) {
            return WifiApplication.connectWPA2(wifi.getSsid(), wifi.getPass());
        }
        return false;
    }

    private static boolean forgetWPA2(String ssid) {
        Log.i("ssid search", ssid);
        WifiManager wm = WifiApplication.getWifiManager();

        List<WifiConfiguration> foundWifis = WifiApplication.getWifiManager().getConfiguredNetworks();
        for(WifiConfiguration wc : foundWifis) {
            wm.removeNetwork(wc.networkId);
            if(wc.SSID.equals(ssid)) {
                wm.removeNetwork(wc.networkId);
                return true;
            }
        }
        return false;
    }

    private static boolean connectWPA2(String ssid, String pass) {
        return forgetWPA2(ssid);
        /*
        WifiConfiguration wc = new WifiConfiguration();
        wc.SSID = "\""+ ssid + "\"";
        wc.preSharedKey  = "\"" + pass + "\"";
        wc.hiddenSSID = true;
        wc.status = WifiConfiguration.Status.ENABLED;
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);

        try {
            wc = setWifiProxySettings(wc);

        }catch(Exception e){
            e.printStackTrace();
        }

        int res = WifiApplication.getWifiManager().addNetwork(wc);
        WifiApplication.getWifiManager().saveConfiguration();
        Log.d("WifiPreference", "add Network returned " + res);
        return WifiApplication.getWifiManager().enableNetwork(res, true);
        */
    }

    public static ArrayList<Wifi> search4LocalWifi() {

        List<ScanResult> foundWifis = WifiApplication.getWifiManager().getScanResults();
        for(ScanResult sr: foundWifis) {
            String capabilities  = sr.capabilities;
            if(capabilities.contains("WPA")) {
                Wifi w = new Wifi(sr.SSID);
                w.setType("WPA");
                w.setBssid(sr.BSSID);
                WifiApplication.setWifiConfiguration(sr.SSID, w);
            }
        }
        return WifiApplication.getWifisList();
    }

    public static WifiConnectionCallback getWifiConnectionCallback() {
        if(_connectionCallback == null) {
            _connectionCallback = new WifiConnectionCallback(){
                @Override
                public void onSuccess() {
                    Log.i("recibidor", "suceso");
                }
                @Override
                public void onError() {
                    Log.i("recibidor", "errorazo");
                }
            };
        }
        return _connectionCallback;
    }

    public static void setWifiConnectionCallback(WifiConnectionCallback callback) {
        _connectionCallback = callback;
    }

    public static WifiApplication getInstance() {
        return _instance;
    }

    public static ApiController getApiController() {
        if(_api == null) {
            _api = new ApiController();
        }
        return _api;
    }

    private static String UUID;

    public static String getUUID(){
        if(UUID == null) {
            String deviceId = Settings.Secure.getString(
                    WifiApplication.getInstance().getContentResolver(),
                    Secure.ANDROID_ID
            );
            UUID = deviceId;
        }
        return UUID;
    }

    public static Object getField(Object obj, String name)
            throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        Field f = obj.getClass().getField(name);
        Object out = f.get(obj);
        return out;
    }

    public static Object getDeclaredField(Object obj, String name)
            throws SecurityException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException {
        Field f = obj.getClass().getDeclaredField(name);
        f.setAccessible(true);
        Object out = f.get(obj);
        return out;
    }

    public static void setEnumField(Object obj, String value, String name)
            throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        Field f = obj.getClass().getField(name);
        f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));
    }

    public static void setProxySettings(String assign , WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException{
        setEnumField(wifiConf, assign, "proxySettings");
    }


    static WifiConfiguration GetCurrentWifiConfiguration(WifiManager manager)
    {
        if (!manager.isWifiEnabled())
            return null;

        List<WifiConfiguration> configurationList = manager.getConfiguredNetworks();
        WifiConfiguration configuration = null;
        int cur = manager.getConnectionInfo().getNetworkId();
        for (int i = 0; i < configurationList.size(); ++i)
        {
            WifiConfiguration wifiConfiguration = configurationList.get(i);
            if (wifiConfiguration.networkId == cur)
                configuration = wifiConfiguration;
        }

        return configuration;
    }

    private static WifiConfiguration setWifiProxySettings(WifiConfiguration wc)
    {
        //get the current wifi configuration
        WifiConfiguration config = wc;
        if(null == config)
            return config;

        try
        {
            //get the link properties from the wifi configuration
            Object linkProperties = getField(config, "linkProperties");
            if(null == linkProperties)
                return config;

            //get the setHttpProxy method for LinkProperties
            Class proxyPropertiesClass = Class.forName("android.net.ProxyProperties");
            Class[] setHttpProxyParams = new Class[1];
            setHttpProxyParams[0] = proxyPropertiesClass;
            Class lpClass = Class.forName("android.net.LinkProperties");
            Method setHttpProxy = lpClass.getDeclaredMethod("setHttpProxy", setHttpProxyParams);
            setHttpProxy.setAccessible(true);

            //get ProxyProperties constructor
            Class[] proxyPropertiesCtorParamTypes = new Class[3];
            proxyPropertiesCtorParamTypes[0] = String.class;
            proxyPropertiesCtorParamTypes[1] = int.class;
            proxyPropertiesCtorParamTypes[2] = String.class;

            Constructor proxyPropertiesCtor = proxyPropertiesClass.getConstructor(proxyPropertiesCtorParamTypes);

            //create the parameters for the constructor
            Object[] proxyPropertiesCtorParams = new Object[3];
            proxyPropertiesCtorParams[0] = "192.168.0.4";
            proxyPropertiesCtorParams[1] = 8888;
            proxyPropertiesCtorParams[2] = null;

            //create a new object using the params
            Object proxySettings = proxyPropertiesCtor.newInstance(proxyPropertiesCtorParams);

            //pass the new object to setHttpProxy
            Object[] params = new Object[1];
            params[0] = proxySettings;
            setHttpProxy.invoke(linkProperties, params);

            setProxySettings("STATIC", config);

            //save the settings
            return config;

        }
        catch(Exception e){
            e.printStackTrace();
        }
        return config;
    }
    void unsetWifiProxySettings()
    {
        WifiManager manager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration config = GetCurrentWifiConfiguration(manager);
        if(null == config)
            return;

        try
        {
            //get the link properties from the wifi configuration
            Object linkProperties = getField(config, "linkProperties");
            if(null == linkProperties)
                return;

            //get the setHttpProxy method for LinkProperties
            Class proxyPropertiesClass = Class.forName("android.net.ProxyProperties");
            Class[] setHttpProxyParams = new Class[1];
            setHttpProxyParams[0] = proxyPropertiesClass;
            Class lpClass = Class.forName("android.net.LinkProperties");
            Method setHttpProxy = lpClass.getDeclaredMethod("setHttpProxy", setHttpProxyParams);
            setHttpProxy.setAccessible(true);

            //pass null as the proxy
            Object[] params = new Object[1];
            params[0] = null;
            setHttpProxy.invoke(linkProperties, params);

            setProxySettings("NONE", config);

            //save the config
            manager.updateNetwork(config);
            manager.disconnect();
            manager.reconnect();
        }
        catch(Exception e)
        {
        }
    }

}
