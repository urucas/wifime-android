package com.urucas.wifime.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.urucas.wifime.R;
import com.urucas.wifime.WifiApplication;
import com.urucas.wifime.interfaces.AddressCallback;
import com.urucas.wifime.interfaces.WifiAddCallback;
import com.urucas.wifime.model.CustomError;
import com.urucas.wifime.model.Wifi;
import com.urucas.wifime.services.ObtainLocation;
import com.urucas.wifime.utils.Utils;

import java.io.IOException;
import java.util.List;

/**
 * Created by Urucas on 8/25/14.
 */
public class AddActivity extends ActionBarActivity {

    private LocationManager locationManager;
    private Criteria criteria;
    private String provider;
    private Location myLocation;
    private LatLng myLatLng;
    private SupportMapFragment mySupportMapFragment;
    private GoogleMap locationMap;

    public static Wifi WIFI;
    private EditText addrEditText;
    private EditText typeEditText;
    private EditText ssidEditText;
    private EditText placeEditText;
    private EditText passEditText;
    private Button saveBtt;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        getSupportActionBar().setTitle(R.string.addwifi);
        getSupportActionBar().setHomeButtonEnabled(true);

        try {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            criteria = new Criteria();

            // Get the name of the best provider
            provider = locationManager.getBestProvider(criteria, true);

            // Get Current Location
            myLocation = locationManager.getLastKnownLocation(provider);

            myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());

        } catch (Exception e) {
            myLatLng = new LatLng(-34.603857, -58.381863);
        }

        FragmentManager myFragmentManager = getSupportFragmentManager();
        mySupportMapFragment
                = (SupportMapFragment) myFragmentManager.findFragmentById(R.id.mapReport);

        locationMap = mySupportMapFragment.getMap();

        locationMap.getUiSettings().setZoomControlsEnabled(false);
        locationMap.getUiSettings().setAllGesturesEnabled(false);

        locationMap.setMyLocationEnabled(false);
        locationMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        locationMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 14));

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(myLatLng);

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.poi);
        markerOptions.icon(icon);

        locationMap.addMarker(markerOptions);

        addrEditText = (EditText) findViewById(R.id.addr);
        typeEditText = (EditText) findViewById(R.id.type);

        ssidEditText = (EditText) findViewById(R.id.ssid);
        ssidEditText.setText(WIFI.getSsid());

        placeEditText = (EditText) findViewById(R.id.place);
        passEditText = (EditText) findViewById(R.id.pass);

        new ObtainLocation(AddActivity.this, myLatLng, new AddressCallback() {
            @Override
            public void onSuccess(Address addr) {

            }

            @Override
            public void onSuccess(String addr) {
                addrEditText.setText(addr);
            }

            @Override
            public void onError() {
                Utils.Toast(AddActivity.this, R.string.errorgettingaddr);
            }
        }).execute();

        saveBtt = (Button) findViewById(R.id.saveBtt);
        saveBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveWifiSettings();
            }
        });
    }

    private void saveWifiSettings() {

        String wifiName = placeEditText.getText().toString();
        if(wifiName.isEmpty()) {
            Utils.Toast(AddActivity.this, R.string.placenameEmpty);
            return;
        }

        String wifiPass = passEditText.getText().toString().trim();
        if(wifiPass.length() < 8 || wifiPass.length() > 63){
            Utils.Toast(AddActivity.this, R.string.passEmpty);
            return;
        }

        String wifiAddr = addrEditText.getText().toString();

        WIFI.setPass(wifiPass);
        WIFI.setName(wifiName);
        WIFI.setAddr(wifiAddr);
        WIFI.setLat(myLatLng.latitude);
        WIFI.setLng(myLatLng.longitude);


        dialog = ProgressDialog.show(AddActivity.this, "", getResources().getString(R.string.addingwifi), true);
        dialog.setCancelable(false);
        dialog.show();

        WifiApplication.getApiController().addWifiSettings(AddActivity.this, WIFI, new WifiAddCallback(){

            @Override
            public void onSuccess(Wifi w) {
                wifiSavedSuccess(w);
            }

            @Override
            public void onError(CustomError error) {
                wifiSavedError(error);
            }
        });
    }

    private void wifiSavedSuccess(Wifi w) {
        dialog.cancel();

        setResult(RESULT_OK);
        finish();
    }

    private void wifiSavedError(CustomError error) {
        dialog.cancel();
        Utils.Toast(AddActivity.this, error.getLocalizedMessage());
    }
}
