package com.urucas.wifime.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.urucas.wifime.R;
import com.urucas.wifime.WifiApplication;

/**
 * Created by Urucas on 8/28/14.
 */
public class HowActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how);

        try {
            getSupportActionBar().setTitle(R.string.howitworks);

        }catch(Exception e){}

        WifiApplication.savePreference("howitworks", true);
    }

}
