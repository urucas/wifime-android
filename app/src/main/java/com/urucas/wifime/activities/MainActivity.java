package com.urucas.wifime.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.urucas.wifime.R;
import com.urucas.wifime.WifiApplication;
import com.urucas.wifime.adapter.WifisAdapter;
import com.urucas.wifime.interfaces.WifiConnectionCallback;
import com.urucas.wifime.interfaces.WifisCallback;
import com.urucas.wifime.model.CustomError;
import com.urucas.wifime.model.Wifi;
import com.urucas.wifime.services.WifiReceiver;
import com.urucas.wifime.utils.Utils;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    private static final int INTENT_ADDACTIVITY = 1;
    private WifisAdapter adapter;
    private ListView listView;
    private WifiReceiver wifiReciver;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle(R.string.app_name);

        WifiApplication.search4LocalWifi();
        WifiApplication.setWifiConnectionCallback(new WifiConnectionCallback() {
            @Override
            public void onSuccess() {
                Utils.Toast(MainActivity.this, R.string.connected);
            }

            @Override
            public void onError() {
                Utils.Toast(MainActivity.this, R.string.errorgettingwifi);
            }
        });

        adapter = new WifisAdapter(
                MainActivity.this,
                R.layout.adapter_wifi_item,
                WifiApplication.getWifisList()
        );

        listView = (ListView) findViewById(R.id.wifisList);
        listView.setAdapter(adapter);
        listView.setItemsCanFocus(true);
        listView.setFocusable(false);
        listView.setFocusableInTouchMode(false);
        listView.setClickable(false);

        searchMatches();

        wifiReciver = new WifiReceiver();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        registerReceiver(wifiReciver, intentFilter);

        boolean firstTime = WifiApplication.getBooleanPreference("howitworks");
        if(firstTime == false) {
            howitworks();
        }

    }

    public void connect(String ssid) {
        if(!WifiApplication.connect(ssid)) {
            Utils.Toast(MainActivity.this, R.string.errorgettingwifi);
            return;
        }
        Utils.Toast(MainActivity.this, R.string.connectingwifi);
    }

    private void searchMatches(){

        dialog = ProgressDialog.show(MainActivity.this, "", getResources().getString(R.string.searchingserver), true);
        dialog.setCancelable(true);
        dialog.show();

        WifiApplication.getApiController().getWifisByMatch(
                WifiApplication.getWifisList(),
                new WifisCallback() {
                    @Override
                    public void onSucccess(ArrayList<Wifi> wifis) {
                        searchMatchesSuccess(wifis);
                    }

                    @Override
                    public void onError(CustomError error) {
                        searchMatchesError(error);
                    }
                }
        );
    }

    private void searchMatchesError(CustomError error) {
        try { dialog.cancel(); } catch(Exception e){}
        Utils.Toast(MainActivity.this, R.string.errorgettingwifilist);
    }

    private void searchMatchesSuccess(ArrayList<Wifi> wifis) {

        try { dialog.cancel(); } catch(Exception e){}

        for(Wifi wc: wifis) {
            String ssid = wc.getSsid();
            Wifi wifi = WifiApplication.getWifiConfiguration(ssid);
            if(wifi != null) {
                wifi.setName(wc.getName());
                wifi.setType(wc.getType());
                wifi.setMatched(true);
                wifi.setPass(wc.getPass());
                WifiApplication.setWifiConfiguration(ssid, wifi);
            }
        }
        updateWifisList(WifiApplication.getWifisList());
    }

    private void updateWifisList(ArrayList<Wifi> wifis) {
        adapter.clear();
        adapter.addAll(wifis);
        adapter.notifyDataSetChanged();
    }

    private void refresh() {

        WifiApplication.search4LocalWifi();
        searchMatches();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }
        if(id == R.id.action_how) {
            howitworks();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addWifiConfig(Wifi w) {

        AddActivity.WIFI = w;
        Intent intent = new Intent(MainActivity.this, AddActivity.class);
        startActivityForResult(intent, INTENT_ADDACTIVITY);
    }

    private void howitworks(){

        Intent intent = new Intent(MainActivity.this, HowActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == INTENT_ADDACTIVITY && resultCode == RESULT_OK) {
            searchMatches();
        }
    }

}
