package com.urucas.wifime.controller;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;
import com.urucas.wifime.WifiApplication;
import com.urucas.wifime.activities.AddActivity;
import com.urucas.wifime.interfaces.AddressCallback;
import com.urucas.wifime.interfaces.WifiAddCallback;
import com.urucas.wifime.interfaces.WifisCallback;
import com.urucas.wifime.model.CustomError;
import com.urucas.wifime.model.Wifi;
import com.urucas.wifime.parser.ErrorParser;
import com.urucas.wifime.parser.WifiParser;
import com.urucas.wifime.parser.WifisParser;
import com.urucas.wifime.services.JSONRequestTask;
import com.urucas.wifime.services.JSONRequestTaskHandler;
import com.urucas.wifime.services.ObtainLocation;
import com.urucas.wifime.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Urucas on 8/23/14.
 */
public class ApiController {

    private static String BASE_URL = "http://wifis.urucas.com/api";

    private boolean isConnected() {
        //checks internet connection
        return Utils.isConnected(WifiApplication.getInstance().getApplicationContext());
    }

    public void getWifisByMatch(ArrayList<Wifi> wifis, final WifisCallback callback) {

        if(!isConnected()) {
            callback.onError(new CustomError("NO_CONNECTION"));
            return;
        }

        String url = BASE_URL + "/matchwifis/";

        JSONArray jsonWifis = new JSONArray();
        for(Wifi w: wifis){
            JSONObject jsonWifi = new JSONObject();
            try {
                jsonWifi.put("ssid", w.getSsid());
                jsonWifi.put("bssid", w.getBssid());
                jsonWifis.put(jsonWifi);
            } catch (JSONException e) { }
        }

        new JSONRequestTask(new JSONRequestTaskHandler() {

            @Override
            public void onSuccess(JSONObject response) {
                if(response.has("error")){
                    CustomError error = ErrorParser.parse(response);
                    callback.onError(error);
                    return;
                }
            }

            @Override
            public void onSuccess(JSONArray result) {
                ArrayList<Wifi> wifis = WifisParser.parse(result);
                callback.onSucccess(wifis);
            }

            @Override
            public void onError(String message) {
                callback.onError(new CustomError(message));
            }

        }).addParam("w", jsonWifis.toString())
                .execute(url);

    }

    public void getWifisByGeo(double lat, double lng, final WifisCallback callback) {

        if(!isConnected()) {
            callback.onError(new CustomError("NO_CONNECTION"));
            return;
        }

        String url = BASE_URL + "/geowifis/";

        new JSONRequestTask(new JSONRequestTaskHandler() {

            @Override
            public void onSuccess(JSONObject response) {
                if(response.has("error")){
                    CustomError error = ErrorParser.parse(response);
                    callback.onError(error);
                    return;
                }
            }

            @Override
            public void onSuccess(JSONArray result) {
                ArrayList<Wifi> wifis = WifisParser.parse(result);
                callback.onSucccess(wifis);
            }

            @Override
            public void onError(String message) {
                callback.onError(new CustomError(message));
            }

        }).addParam("lat", String.valueOf(lat))
                .addParam("lng", String.valueOf(lng))
                .execute(url);
    }

    public void addWifiSettings(AddActivity addActivity, Wifi wifi, final WifiAddCallback callback) {

        if(!isConnected()) {
            callback.onError(new CustomError("NO_CONNECTION"));
            return;
        }

        String url = BASE_URL + "/addwifisettings/";
        JSONObject jsonWifi = new JSONObject();
        try {
            jsonWifi.put("ssid", wifi.getSsid());
            jsonWifi.put("pass", wifi.getPass());
            jsonWifi.put("lat", wifi.getLat());
            jsonWifi.put("lng", wifi.getLng());
            jsonWifi.put("name", wifi.getName());
            jsonWifi.put("type", wifi.getType());
            jsonWifi.put("addr", wifi.getAddr());
            jsonWifi.put("bssid", wifi.getBssid());
            jsonWifi.put("uniqueid", WifiApplication.getUUID());

        } catch (JSONException e) {

            callback.onError(new CustomError(e.getMessage()));
            return;
        }

        new JSONRequestTask(new JSONRequestTaskHandler() {

            @Override
            public void onSuccess(JSONObject response) {
                if(response.has("error")){
                    CustomError error = ErrorParser.parse(response);
                    callback.onError(error);
                    return;
                }
                Wifi wifi1 = WifiParser.parse(response);
                callback.onSuccess(wifi1);
            }

            @Override
            public void onSuccess(JSONArray result) {

            }

            @Override
            public void onError(String message) {
                callback.onError(new CustomError(message));
            }

        }).addParam("w", jsonWifi.toString()).execute(url);
    }
}
