package com.urucas.wifime.parser;

import com.urucas.wifime.model.Wifi;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Urucas on 8/23/14.
 */
public abstract class WifisParser {

    public static ArrayList<Wifi> parse(JSONArray jsonArray) {
        ArrayList<Wifi> wifis = new ArrayList<Wifi>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                Wifi w = WifiParser.parse(jsonArray.getJSONObject(i));
                if(w != null) {
                    wifis.add(w);
                }
            } catch (JSONException e) {
                continue;
            }
        }
        return wifis;
    }
}
