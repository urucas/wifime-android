package com.urucas.wifime.parser;

import com.urucas.wifime.model.Wifi;

import org.json.JSONObject;

/**
 * Created by Urucas on 8/23/14.
 */
public abstract class WifiParser {

    public static Wifi parse(JSONObject jsonObject){

        try {

            String name = jsonObject.getString("name");
            String ssid = jsonObject.getString("ssid");
            String pass = jsonObject.getString("pass");
            double lat = jsonObject.getDouble("lat");
            double lng = jsonObject.getDouble("lng");
            String type = jsonObject.getString("type");
            String addr = jsonObject.getString("addr");
            String bssid = jsonObject.getString("bssid");

            Wifi wifi = new Wifi(ssid);
            wifi.setSsid(ssid);
            wifi.setName(name);
            wifi.setPass(pass);
            wifi.setType(type);
            wifi.setLat(lat);
            wifi.setLng(lng);
            wifi.setBssid(bssid);
            wifi.setAddr(addr);

            return wifi;

        }catch(Exception e) {}

        return null;
    }
}
